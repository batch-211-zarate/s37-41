// s37 -  Express.js - API Development (Part 1)

/*
	
App: Booking System API

Scenario:
	A course booking system apploication where a userr can enroll into a course

Type: Course Booking System (Web App)

Description:
	A course booking system application where a user can enroll into a course
	Allows an admin to do CRUD operations
	Allows users to register into our database

Features:
	- User Login (User Authentication)
	- User Registration

	Customer / Authenticated Users:
	- View Courses (all active courses)
	- Enroll Course

	Admin Users:
	- Add Course
	- Uodate Course
	- Archive/Unarchive a course (soft delete/reactivate the course)
	- View Courses (All courses active/inactive)
	- View / Manage User Accounts**

	All Users(guests, customers, admin)
	- View Active Courses

*/

// Data Model for the Booking System

// Two-way Embedding

	/*
		users
			{
				id - unique identifier for the document,
				firstName,
				lastName,
				email, 
				password,
				mobileNumber,
				isAdmin,
				enrollments: [
					{
						id - document identifier,
						courseId - unique identifier,
						courseName - optional,
						isPaid,
						dateEnrolled
					}
				]
			}

		course 
			{
				id - unique identifier for the document,
				name,
				description,
				price,
				slots,
				isActive,
				createdOn,
				enrollees: [
					{
						id - document identifier
						userId - unique identifier for the users
						isPaid,
						dateEnrolled
					}
				]

			}

	*/

// With Referencing

	/*
		users
			{
				id - unique identifier for the document,
				firstName,
				lastName,
				email, 
				password,
				mobileNumber,
				isAdmin
			}
		
		course 
			{
				id - unique identifier for the document,
				name,
				description,
				price,
				slots,
				isActive,
				createdOn
			}

		enrollment 
			{
				id - document identifier,
				userId - unique identifier for the users
				courseId - unique identifier for the course,
				courseName - optional,
				isPaid,
				dateEnrolled
			}
	*/

/*
	Mini - Activity
		
		1. Create a models folder and create a "Course.js" file to store the schema of our courses
		2. Put the neccessary data types of our fields.
	

*/