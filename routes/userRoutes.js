const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");


// s38 -Express.js - API Development (Part 2)

// Route for checking if the user's email already exists in the database
// pass the  "body" property of our request

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving details of a user
	// Solution 1
// router.post("/details", (req,res) => {
// 	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
// });
	

router.get("/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route to Enroll a User to a course
// router.post("/enroll", (req, res) => {
// 	let data = {
// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	};

// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// });



// Activity s41
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	};

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;