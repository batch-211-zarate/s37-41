const Course = require("../models/Course");
const User = require("../models/User");
const auth = require("../auth");


// Create a new course
	/*
		Steps:
			1. Create a new course object using the mongoose model and the information from the reqBody 
				name 
				description
				price
			2. Save the new Course to the database

	*/

		/*	module.exports.addCourse = (reqBody) => {
				let newCourse = new Course({
					name : reqBody.name,
					description : reqBody.description,
					price : reqBody.price
				});
				return newCourse.save().then((course, err) => {
					if (error) {
						return false;
					} else {
						return true;
					};
				});
			};
		*/


// Refactor the addCourse controller method to implement admin authentication for creating a course.
	//     1. Create a conditional statement that will check if the user is an administrator.
    //     2. Create a new Course object using the mongoose model and the information from the request body and the id from the headers.
    module.exports.addCourse = (data) => {		

		if (data.isAdmin) {

			let newCourse = new Course({
				name: data.course.name,
				description: data.course.description,
				price: data.course.price
			});

			return newCourse.save().then((course, err) => {
				if (err) {
					return false;
				} else {
					return true;
				};
			});
		} else {
			return false;
		};
   }; 


// Retrieve all courses
	/*
		Steps:
			1. Retrieve all the courses from the database
	*/

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};


// Retrieve all Active Courses
	/*
		1. Retrieve all the courses from the database with the property of "isActive" to true
	*/

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};


// Retrieve a Specific Course
	/*
		1. Retrieve the courses that matches the course ID provided from the URL
	*/

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Update a course
	/*
		1. Create a variable "updateCourse" which will be contain the inforrmation retrieved from the request body
	*/

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	});
};

// Archive a course

module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	};
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, err) => {
		if (err) {
			return false;
		} else {
			return ({message: "The course is now inactive", result: course});
		}
	});
};