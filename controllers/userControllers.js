
// s38 -Express.js - API Development (Part 2)

const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");
// bcrypt is a package which allows us to hash our password to add layer of security for our user's details.

// Check if the email already exists
	/*
		Steps:
			1. Use mongoose "find" method to find duplicate emails
			2. Use the "then" method to send a response back to the FE application based on the result of the "find" method
	*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};


// User Registration
	/*
		Steps:
			1. Create a new User objects using the mongoose model and the info from the reqBody
			2. Make sure that the password is encrypted
			3. Save the new User to the database
	*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, err) => {
		if (err) {
			return false;
		} else {
			return true;
		};
	});
};


// User Authentication
	/*
		Steps:
			1. Check the database if the user email exists
			2. Compare the password in the login form with the password stored in the database
			2. Generate/return a JSON Web Token if the user is successfully logged in and return false if not
	*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) }
			} else {
				return false;
			};
		};
	});
};


// Retrieve Users details
	/*
		Steps:
            1. Find the document in the database using the user's ID
            2. Reassign the password of the returned document to an empty string
            3. Return the result back to the frontend/postman
	*/

// Solution 1:
// module.exports.getProfile = (reqBody) => {
// 	return User.findById(reqBody.id).then(result => {
// 		result.password = " ";
// 		return result;	
// 	});
// };

// Solution 2:
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
}

// Enroll user to a class
	/*
		1. Find the document in the database using the user's ID
		2. Add the courseID to the user's enrollment array
		3. Update the document in MongoDB
	*/
	/*
		First, find the user who is enrolling and update his enrollments subdocument array.
		We will push the courseID in the enrollments array

		Second, find the course where we are enrrolling and updates in enrollees subdocument array. We will push the userId in the enrollees array.

		since we will access 2 collection in one action -- we will have to wait for the completion of the action instead of letting Javascript continue line per line

		To be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding.
		
	*/

module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId : data.courseId});

		return user.save().then((user,err) => {
			if (err) {
				return false;
			} else {
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId : data.userId});

		return course.save().then((course,err) => {
			if (err) {
				return false;
			} else {
				return true;
			};
		});
	});

	if (isUserUpdated && isCourseUpdated) {
			return true;
		} else {
			return false;
		};
};

// Activity s41
module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId : data.courseId});

		return user.save().then((user,err) => {
			if (err) {
				return false;
			} else {
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId : data.userId});

		return course.save().then((course,err) => {
			if (err) {
				return false;
			} else {
				return true;
			};
		});
	});

	if (isUserUpdated && isCourseUpdated) {
			return true;
		} else {
			return false;
		};
};