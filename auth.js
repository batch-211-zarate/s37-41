// s38 -Express.js - API Development (Part 2)


/*
	auth.js is our own module which will contain methods to help authorize or restrict users from accessing certain features in our app

*/

const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// JSON Web Tokens
	/*
		- JSON Web Token or JWT  is a way of securely passing information from the server to the frontend or to other parts of server
	*/

module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin,
	};

	return jwt.sign(data, secret, {});
};

// Token Verification
	/*
		Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tempered with
	*/

module.exports.verify = (req,res,next) => {
	// the token is retrieved from the request header
	// this can be provided in postman under
		// Authorization > Bearer Token
	let token = req.headers.authorization;

	if (typeof token !== "undefined") {
		console.log(token);
		token = token.slice(7, token.length);
		// the "slice" method takes only the token from the information sent via request header
		// Bearer y1d0jfjjpofk or Authorization: Bearer <token>
		return jwt.verify(token, secret, (err,data) => {
			if (err) {
				return res.send({auth:"failed"});
			} else {
				// allows the application to proceed with the next middleware function/callback function
				next();
			};
		});
	} else {
		return res.send({auth:"failed"});
	};
};


// Token decryption
	/*
		Open the gift and get the content
	*/

module.exports.decode = (token) => {
	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null;
			} else {
				return jwt.decode(token, {complete:true}).payload;
			};
		});

	} else {
		return null;
	};
};

