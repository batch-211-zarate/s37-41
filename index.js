
const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");

const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes");

const app = express();

mongoose.connect("mongodb+srv://admin123:admin123@project0.8uhvgjn.mongodb.net/s37-41?retryWrites=true&w=majority", 

	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."))

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});

